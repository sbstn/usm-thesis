2.1 (Noviembre 2015)

- Mejor control de la numeración de títulos y subtítulos en el documento y en la tabla de contenidos.
- Mejor Tabla de Figuras: Elimina la fuente (en caso de incluirla en la descripción de la figura).

2.0 (Noviembre 2015)

- Corrección de errores de leyendas de figuras alineadas a la izquierda.
- Cambio de algunos paquetes obsoletos.
- Mayor compatibilidad con sistemas Windows (y otros).
- Nuevos logos.
- Corrección de errores menores.
- Segundo correferente (opcional) agregado para tesis de magister.

1.9 (Mayo 2015)

- Compatibilidad con otros sistemas.
- Incorporación de una descripción de las Normas USM para la presentación de Memorias y Tésis.

1.8 (April 2014)

- LyX Templates using (sty) templates

1.7 (March 2014)

- USM Memoria Template (sty)

1.6 (July 2013)

- Bugfixes & Spelling

1.5 (July 2013)

- Bugfixes

1.4 (July 2013)

- Bugfixes

1.3 (July 2013)

- Bugfixes

1.2 (July 2013)

- Bugfixes

1.1 (May 10, 2013)

- Added TikZ and PGF support

1.0 (April 16, 2013)

- RELEASE

0.8 (April 14, 2013)

- Agregado archivo usm.bst para formatos de citas bibliográficas USM.

0.7 (April 10, 2013)

- Plantilla en LyX

0.6 (March 25, 2013)

- Cambios menores, ayuda para la configuración.

0.5

- Cambio de las denominaciones de Figuras, Tablas, Anexos e Índices para Español (Chile).
- Opciones para configurar los títulos de los capítulos
- Marca de Agua con el Escudo de la Universidad

0.4:

- Ejemplos para la incorporación de Figuras y Tablas

0.3:

- Numeración de páginas en Romanos para las páginas del Preámbulo y Páginas de Índices.

0.2:

- Archivo de Bibliografía (bibtex) agregado.

0.1:

- Corrección de algunos errores menores.
