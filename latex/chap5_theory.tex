%!TEX root = memoria.tex

\chapter{ Marco Teórico }

Este capítulo revisará la base conceptual para
construir el marco teórico que permita contextualizar la
implementación del sistema de información. Para considerar la implementación 
como exitosa es necesario identificar en qué parte del modelo de negocio puede
generar una ventaja competitiva, también es necesario entender cómo medir la 
adopción y satisfacción del sistema por parte de los usuarios. Una mala adopción puede elevar los costos de implementación y disminuir los beneficios esperados del sistema.

Además se entenderán los principales componentes de los Sistemas de Información para la Gestión 
y cómo pueden ser permanentemente modificados y mejorados. 

\section{ Métodos y herramientas para generar Modelos de Negocios. }

Una de las herramientas recientes, pero ampliamente aceptadas para apoyar la Misión corporativa es el \emph{Business Model Canvas,} \citep{osterwalder_business_2010, blank_why_2013}, el cual mediante nueve dimensiones
permite tener una perspectiva clara y resumida de cómo y con qué recursos la empresa generará valor (beneficios) a sus clientes y utilidades para la empresa \citep{university_of_economics_in_bratislava_relationship_2016} 

Las nueve perspectivas o componentes del \emph{Business Model Canvas} que deben identificarse son:

\begin{itemize}
	\item[Segmento de Clientes:]
        Se definen los grupos de personas o empresas a los cuales se intentará satisfacer.      
    \item[Propuesta de Valor:]
	    	Describe el mix de productos o servicios que crean valor a cada segmento de clientes.
    \item[Canales:]
        Por qué medio la empresa entrega o provee su propuesta de valor a cada segmento de clientes.
    \item[Relación con los Clientes:]
        En esta vertical se analiza y define el tipo de relación que la empresa tendrá con los segmentos de clientes. 
        Como ejemplo se puede mencionar una relación personalizada, automatizada, autoservicio o co-creación.      
    \item[Flujo de ingresos:]
	      Es el activo circulante que la empresa genera de cada segmento de clientes.
    \item[Recursos Clave:]
	      Son los activos de capital más importantes para hacer funcionar al modelo de negocio.
    \item[Actividades Clave:]
	      Las actividades más importantes que la empresa debe realizar especialmente bien para que le modelo de negocio funcione.
    \item[Asociaciones Clave:]
	      Describe a los proveedores y alianzas que permiten a la empresa tener un modelo funcional estable.
    \item[Estructura de Costos:]
        Incluye todos los costos adquiridos para hacer funcionar al modelo de negocio.
\end{itemize}


\section{ Métricas de satisfacción de usuarios. }

Para evaluar la implementación por parte de los usuarios es necesario conocer las teorías existentes para modelar y probar la aceptación de herramientas tecnológicas. Desde hace más de tres décadas y hasta la actualidad el modelo TAM \citep{surendran_technology_2012} ha propuesto un modelo basado en dos factores; utilidad percibida y facilidad de uso percibida. La utilidad percibida se refiere a la probabilidad subjetiva del usuario de que usando una aplicación específica su rendimiento en el trabajo o personal mejorará, mientras que la facilidad de uso percibida (PEOU) es definida como el grado en que el usuario percibe que usar la aplicación será libre de esfuerzo \citep{surendran_technology_2012}. Es un modelo sencillo el cual ha sido sujeto de numerosas adiciones, como la incorporación del reconocimiento social o modificaciones específicas para aplicaciones web, pero su base continúa siendo de la suma de los factores descritos.

Otra medida de satisfacción, más pensada en satisfacción de clientes (internos o externos), es el \emph{Net Promote Score} (NPS). En su publicación original es indicado como \emph{El único número que necesitas aumentar} \citep{reichheld_one_2003}. Este indicador es sencillo de calcular; el usuario o cliente debe responder, usando una escala discreta de 0 a 10 la pregunta ¿Con qué probabilidad recomendarías esta [empresa, producto o servicio] a un [amigo, familiar o colega]?.

Los resultados se clasifican en tres grupos: [0 - 6] para detractores, [7-8] para promotores pasivos y [9 - 10] para promotores activos. Lugo el indicador es calculado como el porcentaje de promotores activos menos el porcentaje de los detractores.

Se sugiere medir este indicador en intervalos de tiempo constantes para comparar y cuantificar mejoras. En el contexto de desarrollo moderno de aplicaciones, donde, como veremos, la evolución permanente de las aplicaciones es la norma, medir en forma constante el NPS es algo que puede ayudar a guiar y validar las mejoras en el desarrollo de aplicaciones. 


\section{ Sistemas de Información para la Gestión. }

El propósito de esta sección es entregar una idea general del uso actual y 
potencial que tienen las herramientas de desarrollo de soluciones de información y como pueden ayudar 
a gestionar el Modelo de Negocio.

\subsection{Sistemas de información y la Organización}

La teoría de general de sistemas (TGS), inicialmente formulada por el biólogo
alemán Ludwig von Bertalanffy en 1925, es un método adecuado para analizar y
entender sistemas complejos que en este trabajo serán la Organización y los
Sistemas de Información (SI).

La TGS analiza las interacciones internas y externas de un fenómeno con el
objetivo de entender y explicar los fenómenos que suceden en la realidad y
extender tales interacciones para predecir la conducta futura de la realidad o parte
de ella \citep{BibUTFSM2008}

Si bien existen varias definiciones de sistemas, en el sentido más amplio, se
puede decir que un sistema es un conjunto de partes que interactúan entre ellas y el
medio externo para lograr un objetivo en común \citep{kendall_alisis_2011}.
Luego es lógico pensar que un sistema pude estar compuesto por otros sistemas. 
Es aquí donde aparece lo que se conoce como sistemas complejos, que se descomponen en 
subsistemas interrelacionados en varias escalas que en el nivel inferior solo tiene a sus
componentes elementales. Pero se debe tener presente que todo sistema complejo
evolucionó desde un sistema más simple. Esta evolución es más favorable para su
éxito si se pasa por estados estables durante el desarrollo \citep{BibUTFSM2008}.

En este contexto, el objetivo de un Sistema de Información es apoyar el proceso de toma de
decisiones en una Organización \citep{BibUTFSM2008,kendall_alisis_2011}. Pero la organización también es un sistema de personas y recursos que busca maximizar el valor de si misma. Por lo tanto, un
Sistema de Información será un subsistema de la organización que apoyará a la
organización a cumplir su objetivo.

Los SI están formados por personas, hardware, software, documentación y
procesos. El subsistema formado por los programas, archivos y procedimientos es
lo que se denomina aplicación de Sistema de Información. Estas aplicaciones pueden ser aplicadas a
ventas, contabilidad o compras \citep{kendall_alisis_2011}. Por lo tanto, como los SI dan apoyo a las
diferentes áreas de la organización, es fundamental estudiar cómo funcionan las
diferentes áreas y entender qué procesos requieren información y cuál es la
información relevante que permite tomar decisiones oportunas alineadas con la
estrategia corporativa y Modelo de Negocio.

\subsection{ Metodologías e Infraestructuras Modernas para Desarrollo de Software}

Hoy contamos con herramientas y artefactos que permiten a equipos pequeños, compuestos por perfiles de negocio y otros con habilidades informáticas \citep{beck2001agile}, entregar y evolucionar software en periodos cortos de tiempo, semanas o meses y desplegar estas aplicaciones en entornos a escala global a costos bajos, sin necesidad de invertir en infraestructura y pagando solo por los recursos de hardware efectivamente utilizados \citep{fox_desarrollando_2015}. Para el caso de una pyme de alcance nacional, estos costos normalmente no superan los 50 dólares mensuales por concepto de infraestructura digital y atender a cientos de clientes/usuarios.

Por el lado de los frameworks, cada vez se establecen y acuerdan convenciones que permiten a los equipos de desarrollo enfocarse en solucionar problemas de negocio y no invertir tiempo en detalles técnicos de bajo nivel. Ejemplos de estos frameworks se indican en la siguiente tabla: 

\begin{table}[ht]
	\centering
	\caption{Ejemplos de Frameworks Open Source de desarrollo de aplicaciones web}
	\begin{tabular}{|p{3cm}|p{2cm}|p{5cm}|}
		\hline
		\textbf{Framework} & \textbf{Lenguaje} & \textbf{Aplicaciones} \\
        \hline
        \hline
		Django               & Python             & CornerShop, Youtube, Spotify \\
        \hline
		Rails                & Ruby                & Github, Tweeter, Basecamp  \\
		\hline
		Ember                & Javascript          & LinkedIn, Netflix, Zendesk  \\
		\hline
		Angular              & Javascript          & Idiegogo, Deutsche Bank, Microsoft LUIS  \\
		\hline
	\end{tabular}
\end{table}

Todos los frameworks mencionados en la anterior tabla tienen casos de éxito que soportan y escalan a millones de usuarios y cualquiera de ellos puede apoyar procesos empresariales. Un caso excepcional es el framework \emph{Bootstrap}, usado en millones de sitios \cite{jakobus_mastering_2018}, el cual simplifica y acorta el proceso para entregar aplicaciones de alta usabilidad.
